process RUN_RESFINDER4 {
    container "anabugseq/resfinder"
    publishDir "${params.output}/resfinder4", mode: "copy"
    tag { id }

    when:
    params.resfinder4

    input:
    tuple val(id), path(sequence)
    path(database)

    output:
    tuple val(id), path("${id}.report_dir"), emit: report_dir

    script:
    """
    python /resfinder/run_resfinder.py -db_res ${database} -ifa ${sequence} -o ${id}.report_dir -acq
    """
}