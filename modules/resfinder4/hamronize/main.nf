process HAMRONIZE_RESFINDER4 {
    container "quay.io/biocontainers/hamronization:1.0.3--py_0"
    publishDir "${params.output}/amrfinderplus", mode: "copy"
    tag { id }

    when:
    params.resfinder4

    input:
    tuple val(id), path(sequence), path(report_dir)

    output:
    tuple val(id), path("${id}.hamronize_resfinder4.json"), emit: json

    script:
    """
    hamronize resfinder4     --analysis_software_version 4 \
                            --reference_database_version 7562716b99c6 \
                            --input_file_name ${sequence} \
                            --format json \
                            ${report_dir}/ResFinder_results_tab.txt \
                            > ${id}.hamronize_resfinder4.json
    """
}