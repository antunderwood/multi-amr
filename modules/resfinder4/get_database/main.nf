process GET_RESFINDER4_DB {
    container "anabugseq/resfinder"
    
    when:
    params.resfinder4

    output:
    path  "database", emit: database

    script:
    """
    curl https://bitbucket.org/genomicepidemiology/resfinder_db/get/7562716b99c6.zip --output database.zip
    unzip -j -d database database.zip
    """
}
