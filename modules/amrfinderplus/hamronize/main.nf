process HAMRONIZE_AMRFINDERPLUS {
    container "quay.io/biocontainers/hamronization:1.0.3--py_0"
    publishDir "${params.output}/amrfinderplus", mode: "copy"
    tag { id }

    when:
    params.amrfinderplus

    input:
    tuple val(id), path(sequence), path(report)

    output:
    tuple val(id), path("${id}.hamronize_amrfinderplus.json"), emit: json

    script:
    """
    hamronize amrfinderplus --analysis_software_version 3.10.16 \
                            --reference_database_version 2021-09-30.1 \
                            --input_file_name ${sequence} \
                            --format json \
                            ${report} \
                            > ${id}.hamronize_amrfinderplus.json
    """
}