process SUMMARIZE_HAMRONIZATION {
    tag "hAMRonize summarize"
    container "quay.io/biocontainers/hamronization:1.0.3--py_0"
    publishDir "${params.output}/hamronize", mode: "copy"

    input:
    path(hamronize_reports)

    output:
    path("harmonization_summary.json")

    script:
    """
    hamronize summarize ${hamronize_reports} -t json -o harmonization_summary.json
    """
}