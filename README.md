A simple Nextflow DSL2 pipeline to process multiple fasta sequences through multiple AMR software and hAMRonize the outputs

## Usage

**Requirements** Your computer needs to have docker or singularity installed

**CLIMB-AMR Hackathon 2021**
On the amr-hackathon-1/2 VM

1. Create a conda env for installing nextflow
```bash
conda create -n multi-amr nextflow
conda activate multi-amr
```
2. Test the pipeline

```bash
nextflow run https://gitlab.com/antunderwood/multi-amr -r main -profile singularity,test
```
3. Run the pipeline on your own dataset

```bash
nextflow run https://gitlab.com/antunderwood/multi-amr --rgi --amrfinderplus --input path/to/your/*.fasta -profile singularity
```

## Other environments
To run with docker rather than singularity use `-profile docker`
Rather than specifying each software via the command you can use a user config file e.g [`user.config`](user.config) and alter the values to true/false to determine which AMR software to run.