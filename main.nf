nextflow.enable.dsl = 2

// Don't overwrite global params.modules, create a copy instead and use that within the main script.
def module_params = params.modules.clone()

include { GET_AMRFINDERPLUS_DB }    from './modules/amrfinderplus/get_database/main' addParams( options: module_params['amrfinderplus'] )
include { RUN_AMRFINDERPLUS }       from './modules/amrfinderplus/run/main'          addParams( options: module_params['amrfinderplus'] )
include { HAMRONIZE_AMRFINDERPLUS } from './modules/amrfinderplus/hamronize/main'    addParams( options: module_params['amrfinderplus'] )
include { GET_RESFINDER4_DB }       from './modules/resfinder4/get_database/main'    addParams( options: module_params['resfinder4'] )
include { RUN_RESFINDER4 }          from './modules/resfinder4/run/main'             addParams( options: module_params['resfinder'] )
include { HAMRONIZE_RESFINDER4 }    from './modules/resfinder4/hamronize/main'       addParams( options: module_params['resfinder'] )
include { GET_CARD_DB }             from './modules/rgi/get_database/main'           addParams( options: module_params['rgi'] )
include { RUN_RGI }                 from './modules/rgi/run/main'                    addParams( options: module_params['rgi'] )
include { HAMRONIZE_RGI }           from './modules/rgi/hamronize/main'              addParams( options: module_params['rgi'] )
include { SUMMARIZE_HAMRONIZATION } from './modules/hamronize/summarize/main'        addParams( options: module_params['harmonize'] )

// Check mandatory parameters
if (params.input) { ch_input = file(params.input) } else { exit 1, 'Input not specified!' }

workflow {
    sample_id_and_seqs = Channel
        .fromPath(params.input)
        .map{ file -> tuple (file.baseName.replaceAll(/\..+$/,''), file)}
        .ifEmpty { error "Cannot find any files matching: ${params.input}" }


    GET_AMRFINDERPLUS_DB()
    RUN_AMRFINDERPLUS(sample_id_and_seqs, GET_AMRFINDERPLUS_DB.out.database)
    HAMRONIZE_AMRFINDERPLUS(sample_id_and_seqs.join(RUN_AMRFINDERPLUS.out.tsv))

    GET_RESFINDER4_DB()
    RUN_RESFINDER4(sample_id_and_seqs, GET_RESFINDER4_DB.out.database)
    HAMRONIZE_RESFINDER4(sample_id_and_seqs.join(RUN_RESFINDER4.out.report_dir))

    GET_CARD_DB()
    RUN_RGI(sample_id_and_seqs, GET_CARD_DB.out.database)
    HAMRONIZE_RGI(sample_id_and_seqs.join(RUN_RGI.out.tsv))

    SUMMARIZE_HAMRONIZATION(
        Channel.of()
            .mix(HAMRONIZE_AMRFINDERPLUS.out.json.map{ it[1] })
            .mix(HAMRONIZE_RESFINDER4.out.json.map{ it[1] })
            .mix(HAMRONIZE_RGI.out.json.map{ it[1] })
            .collect()
    )
}
